# Zootopian Express
This is a simple script that will create some awesome Zootopian Express tickets. You can see a sample in the output directory. 



**Disney owns every single right to Zootopia** and I am only doing this for fun. I have some boys that wanted a Zootopia birthday party so I decided to create this so we could make all their guests a ticket. There was somewhere online that my wife saw a woman had a site that was doing the same but my wife never recieved the tickets. So I decided to dedicate an hour and write this script. Look at the script as it is pretty self explanatory. I am writing a big image to the tmp folder and then putting the smaller tickets in the **output** folder. They are bmp format so you may have to convert them. If you are using a *nix machine and have imagemagick you can just use:

```mogrify -format png *.bmp``` 

when you are in that folder. 

Have fun. 