"""
Zootopian Express
Jared Henry
jhenry6@gmail.com 

A simple script to create some tickets. You can read the names from a 
file if you would like. I just put them in as a list. 

Requires:

python2
imagemagick
python-resize-image
pillow

If you see a problem you can fix or make it better than go for it. ;)
"""

from PIL import Image, ImageDraw, ImageFont
from resizeimage import resizeimage

def main(passenger_name,seat):
    """
    Main function of the application. This will read in
    the 'ticket.png' as a template. Off of this we will 
    create another image of the overlay information. When
    adding the text I did the 'y' coordinates with a formula
    just because how it is show in Acorn on the Mac. 
    """
    base = Image.open('ticket.png').convert('RGBA')
    txt = Image.new('RGBA', base.size)
    base_fnt = ImageFont.truetype('Ticketing.ttf', 40)
    large_fnt = ImageFont.truetype('Ticketing.ttf', 124)
    d = ImageDraw.Draw(txt)
    out = Image.alpha_composite(base, txt)
    d.text((110,(804-472)),passenger_name, font=base_fnt, fill=(0,0,0))
    d.text((110,(804-332)),departure, font=base_fnt, fill=(0,0,0))
    d.text((110,(804-194)),destination, font=base_fnt, fill=(0,0,0))
    d.text((844,(804-472)),gate, font=base_fnt, fill=(0,0,0))
    d.text((490,(804-338)),arrival, font=base_fnt, fill=(0,0,0))
    d.text((1078,(804-329)),seat, font=large_fnt, fill=(0,0,0))
    out = Image.alpha_composite(base, txt)
    out.save("./tmp/{}.bmp".format (passenger_name.replace(" ","_")))
    with open("./tmp/{}.bmp".format (passenger_name.replace(" ","_")), 'r+b') as f:
        with Image.open(f) as image:
            cover = resizeimage.resize_cover(image,[720,302])
            cover.save("./outputs/{}.bmp".format (passenger_name.replace(" ","_")), image.format)

if __name__ == '__main__':
    departure = "9-31-16 2:00 PM"
    arrival = "9-31-16 4:30 PM"
    destination = "Some Awesome Event"
    gate ="Gate C"
    passengers = ['Jared Henry',
                  'Bob Henry']
    seat_number = 13
    for passenger in passengers:
        main(passenger, str(seat_number))
        seat_number += 1